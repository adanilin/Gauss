###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LbAmpGen
################################################################################
gaudi_subdir(LbAmpGen v2r0)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unused-parameter -O3")

gaudi_add_module(LbDtoKpipipi
                 src/DtoKpipipi.cpp)
gaudi_add_module(LbDtopiKpipi
                 src/DtopiKpipi.cpp)
gaudi_add_module(LbDtoKKpipi
                 src/DtoKKpipi.cpp)
gaudi_add_module(LbLctoppipi
                 src/Lctoppipi.cpp)
gaudi_add_module(LbLctopKK
                 src/LctopKK.cpp)


add_custom_target(ampgen_source_files 
                  COMMAND ${env_cmd} --xml ${env_xml}
                      ${CMAKE_CURRENT_SOURCE_DIR}/utils/makeModels.py
                  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/cmt)
