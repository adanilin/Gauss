/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef AMPGEN_COMPILERWRAPPER_H
#define AMPGEN_COMPILERWRAPPER_H

#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace AmpGen
{
  class CompiledExpressionBase; 

  class CompilerWrapper
  {
  public:
    CompilerWrapper( const bool& verbose=false);
    void generateSource( const CompiledExpressionBase& expression, const std::string& fname);
    bool compile( CompiledExpressionBase& expression, const std::string& fname=""); 
    bool compile( std::vector<CompiledExpressionBase*>& expression, const std::string& fname=""); 
    void compileSource(const std::string& fname, const std::string& oname );
    void setVerbose() { m_verbose = true ; } 
    void preamble(std::ostream& os ) const ; 
  private:
    std::vector<std::string> m_includes; 
    bool                     m_verbose;
    std::string              m_cxx;
    std::string generateFilename();
  };
} // namespace AmpGen
#endif
