###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: AmpGen
################################################################################
gaudi_subdir(AmpGen v1r2)

find_package(ROOT COMPONENTS RIO Hist Matrix Graf Minuit2 Tree MathCore Physics MathMore)
find_package(TBB REQUIRED)
find_package(GSL REQUIRED)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp -Wno-unused-function -Wno-unused-parameter -ldl")

include_directories(SYSTEM ${ROOT_INCLUDE_DIRS})

gaudi_add_library(AmpGen
                  src/*.cpp
                  src/Lineshapes/*.cpp
                  NO_PUBLIC_HEADERS
		  INCLUDE_DIRS ROOT
                  LINK_LIBRARIES ROOT TBB GSL)

set(AMPGEN_CXX ${CMAKE_CXX_COMPILER} CACHE FILEPATH "This should be the path to compiler (use which c++ for macOS)" )
set(AMPGEN_CXX ${CMAKE_CXX_COMPILER} FILEPATH "This should be the path to compiler (use which c++ for macOS)" )
gaudi_env(SET AMPGEN_CXX ${CMAKE_CXX_COMPILER})

foreach(app ampGenerator)
  gaudi_add_executable(${app} apps/${app}.cpp
                       LINK_LIBRARIES AmpGen)
endforeach()

      
