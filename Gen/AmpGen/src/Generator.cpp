/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AmpGen/Generator.h"
#include <TRandom3.h>

using namespace AmpGen;

extern "C" void AmpGen::PyGenerate(const char* eventType, double* out, const unsigned int size)
{
  EventType type( split( std::string(eventType),' ') ); 
  INFO( type << " generating: " );
  auto phsp = Generator<PhaseSpace>(type, new TRandom3() );
  auto events = phsp.generate( size,0 ); 
  for( size_t i = 0 ; i < events.size(); ++i ){
    for( size_t j = 0 ; j < events[i].size(); ++j)
      out[events[i].size() * i + j] = events[i][j]; 
  }
}
