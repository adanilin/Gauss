###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LbHerwigpp
################################################################################
gaudi_subdir(LbHerwigpp v3r1)

gaudi_depends_on_subdirs(Gen/Generators)

find_package(ThePEG)
find_package(Herwig++)

if (HERWIG++_FOUND)

  find_package(Boost)
  find_package(HepMC)
  include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS} ${THEPEG_INCLUDE_DIRS})

  gaudi_add_module(LbHerwigpp
                   src/component/*.cpp
                   INCLUDE_DIRS ThePEG
                   LINK_LIBRARIES ThePEG GeneratorsLib)
  
  if(TARGET GSL::gsl)
    set(gsl_target GSL::gsl)
  endif()
  string(REPLACE "-Wl,--no-undefined" "" CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}")
  gaudi_add_library(LbHerwigppRandom
                    src/Lib/*.cpp
                    NO_PUBLIC_HEADERS
                    LINK_LIBRARIES ThePEG GaudiKernel ${gsl_target})

else()
  message(WARNING "missing Herwig++: cannot build LbHerwigpp")
endif()
