###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LbMadgraph
################################################################################
gaudi_subdir(LbMadgraph v1r0)

gaudi_depends_on_subdirs(Gen/LbHard)

find_package(Pythia8 COMPONENTS pythia8)

find_package(Madgraph)

find_package(LHAPDF)

include_directories(SYSTEM ${PYTHIA8_INCLUDE_DIRS})

gaudi_install_python_modules()

gaudi_add_module(LbMadgraph
                 src/component/*.cpp
                 LINK_LIBRARIES LbHardLib)

gaudi_env(SET MADGRAPHEXE ${MADGRAPH_EXE})

gaudi_env(SET MADGRAPHPDF ${MADGRAPH_PDF})

gaudi_env(SET MADGRAPHVER ${MADGRAPH_VERSION})

gaudi_env(PREPEND PATH ${LHAPDF_BINARY_PATH}) 

set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wno-suggest-override")
