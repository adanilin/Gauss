#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

gaudirun.py '$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py' '$APPCONFIGOPTS/Gauss/DataType-2016.py' '$APPCONFIGOPTS/Gauss/RICHRandomHits.py' '$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py' '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py' '$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py' '$LBMADGRAPHROOT/options/70000000.py' '$LBMADGRAPHROOT/options/MadgraphPythia8.py' '$LBMADGRAPHROOT/options/example_job.py'