/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// access GenXicc common MTYPEOFXI 
#include "LbGenXicc/Mtypeofxi.h"

// set pointer to zero at start
Mtypeofxi::MTYPEOFXI* Mtypeofxi::s_mtypeofxi =0;

// Constructor
Mtypeofxi::Mtypeofxi() { }

// Destructor
Mtypeofxi::~Mtypeofxi() { }

// access mgenxi in common
int& Mtypeofxi::mgenxi() {
  init(); // check COMMON is initialized
  return s_mtypeofxi->mgenxi;
}
