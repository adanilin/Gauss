/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// access GenXicc common Usertran ! F. Zhang 01-04-11
#include "LbGenXicc/Usertran.h"

// set pointer to zero at start
Usertran::USERTRAN* Usertran::s_usertran =0;

// Constructor
Usertran::Usertran() { }

// Destructor
Usertran::~Usertran() { }

// F. Zhang 01-04-11
// access ishower in common
//int& Usertran::ishower() {
//  init(); // check COMMON is initialized
//  return s_usertran->ishower;
//}

// access idpp in common
int& Usertran::idpp() {
  init(); // check COMMON is initialized
  return s_usertran->idpp;
}



