###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################# Package: SuperChic2
################################################################################
gaudi_subdir(SuperChic2 v1r0)

gaudi_depends_on_subdirs(Gen/Generators)

string(REPLACE "-pedantic" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Wall" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Wextra" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-Werror=return-type" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/common)

gaudi_add_library(SuperChic2
                  src/main/*.f src/intf/*.f src/init/*.f
                  PUBLIC_HEADERS SuperChic2
                  LINK_LIBRARIES GeneratorsLib pythia6forgauss)
