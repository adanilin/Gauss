/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Generated automatically with Gen/SuperChic2/python/SuperChic2.py.
// Do not manually modify. Based on superchicv2.04.
extern "C" {

  extern struct {
    double s[1], rts[1], yx[1], mx[1];
  } vars_;

  extern struct {
    int sfaci[1], isurv[1];
  } survin_;

  extern struct {
    char intag[100];
  } in_;

  extern struct {
    char pdfname[100];
    int pdfmember[1];
  } pdfint_;

  extern struct {
    int proc[1], dps[1];
    double sym[1];
    int hel[1];
  } process_;

  extern struct {
    char outtag[100];
  } out_;

  extern struct {
    double xl[10], xu[10], acc[1];
    int ndim[1], ncall[1], itmx[1], nprn[1];
  } bveg1_;

  extern struct {
    double prec[1];
  } prec_;

  extern struct {
    int ncall1[1], inccall[1], itend[1], iseed[1];
  } veg_;

  extern struct {
    int s2int[1];
  } nsurv_;

  extern struct {
    int genunw[1];
  } genunw_;

  extern struct {
    double wtmax[1];
    int readwt[1];
  } wtmax_;

  extern struct {
    double ymax[1], ymin[1];
  } yrange_;

  extern struct {
    double mmax[1], mmin[1];
  } mrange_;

  extern struct {
    int gencuts[1];
  } gencut_;

  extern struct {
    int scorr[1];
  } corr_;

  extern struct {
    double ptamin[1], ptbmin[1], etaamin[1], etabmin[1], etaamax[1], 
      etabmax[1], ptamin3[1], ptbmin3[1], ptcmin3[1], etaamin3[1], etabmin3[1], 
      etacmin3[1], etaamax3[1], etabmax3[1], etacmax3[1], ptamin4[1], 
      ptbmin4[1], ptcmin4[1], ptdmin4[1], etaamax4[1], etabmax4[1], 
      etacmax4[1], etadmax4[1], etaamin4[1], etabmin4[1], etacmin4[1], 
      etadmin4[1], ptamin6[1], ptbmin6[1], ptcmin6[1], ptdmin6[1], ptemin6[1], 
      ptfmin6[1], etaamax6[1], etabmax6[1], etacmax6[1], etadmax6[1], 
      etaemax6[1], etafmax6[1], etaamin6[1], etabmin6[1], etacmin6[1], 
      etadmin6[1], etaemin6[1], etafmin6[1];
  } cuts_;

  extern struct {
    double rjet[1];
    char jalg[10];
  } jetalg_;

  extern struct {
    int pdgid[20];
  } pdg_;

  extern struct {
    double gf[1], v[1], mt[1], mb[1], mw[1], me[1], mtau[1], alpha[1];
  } ewpars_;

  extern struct {
    double mmu[1], mpsi[1], m2b[1], mchi[1], mups[1], mpsip[1], mc[1];
  } masses_;

  extern struct {
    double mchic0[1], mchib0[1];
  } masses0_;

  extern struct {
    double mp[1];
  } pmass_;

  extern struct {
    double mpip[1];
  } mpi_;

  extern struct {
    double mkp[1];
  } mkp_;

  extern struct {
    double width[1];
    int fwidth[1];
  } widths_;

  extern struct {
    int histol[1], i[1], iinc[1], itmx1[1], j[1], k[1], ncallu[1], nhistmax[1], 
      outl[1], rebin[1];
    double avgi[1], avgi1[1], avgio[1], beta[1], chi2a[1], etadmax3[1], 
      etadmin3[1], ptdmin3[1], randum[1], sd[1], sd1[1], sdo[1], t2[1];
    int entryp[1], exitp[1];
    char dum[100];
  } sccom_;

  extern struct {
    double wmax[1];
    int calcmax[1], iw[1];
  } wmax_;

  extern struct {
    double ren[1];
    int unw[1], nev[1], evnum[1];
  } unweighted_;

  extern struct {
    int nup[1], idprup[1];
    double xwgtup[1], scalup[1], aqedup[1], aqcdup[1];
    int idup[500], istup[500], mothup[500][2], icolup[500][2];
    double pup[500][5], vtimup[500], spinup[500];
  } hepeup_;

  extern struct {
    double evrec[10][10][2000000];
  } rec_;

  extern struct {
    char erec[10];
  } record_;

  void sbr_superchic__();

  extern struct {
    int isurv[1];
    double irts[1], iisurv[1], ipdfmember[1];
    int entryp[1], exitp[1];
    char iintag[100], ipdfname[100], dum[100];
  } initcom_;

  void sbr_init__();

}
