/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PDF_BASE_HH
#define PDF_BASE_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:56 GMT

#include "Mint/IPdf.h"
#include "Mint/EventAccess.h"
#include "Mint/IEventList.h"
#include "Mint/IReturnReal.h"

namespace MINT{

  template<typename EVENTS>
    class PdfBase : virtual public IPdf<EVENTS>, public EventAccess<EVENTS>{
  public:
  PdfBase(IEventList<EVENTS>* erptr = 0)
    : EventAccess<EVENTS>(erptr)
      {}
  PdfBase(IEventAccess<EVENTS>* erptr)
    : EventAccess<EVENTS>(erptr)
      {}
  PdfBase(const PdfBase& other)
    :  IBasicEventAccess<EVENTS>()
      , IEventAccess<EVENTS>()
      , IReturnReal()
      , IGetRealEvent<EVENTS>()
      , IPdf<EVENTS>()
      , EventAccess<EVENTS>(other)
      {}

   IPdf<EVENTS>* Clone() const{
     return new PdfBase(*this);
   }

    double getVal() override =0;
    double RealVal() override {
      return getVal();
    }

    void beginFit() override {};
    void parametersChanged() override {};
    void endFit() override {};

    virtual double getNewVal(){
      parametersChanged();
      return getVal();
    }

    virtual ~PdfBase(){};
  };

}//namespace MINT
#endif
//
