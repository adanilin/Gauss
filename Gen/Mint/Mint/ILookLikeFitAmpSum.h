/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MINTDALITZ_ILOOKLIKE_FITAMPSUM_HH
#define MINTDALITZ_ILOOKLIKE_FITAMPSUM_HH

#include "Mint/ILookLikeFitAmpSum.h"
#include "Mint/IIntegrationCalculator.h"
#include "Mint/IntegCalculator.h"
#include "Mint/IGetRealEvent.h"
#include "Mint/IDalitzEvent.h"
#include "Mint/IFastAmplitudeIntegrable.h"

#include "Mint/counted_ptr.h"
#include "Mint/DalitzBWBoxSet.h"
#include "TRandom.h"
#include <iostream>

class ILookLikeFitAmpSum
: virtual public MINT::IGetRealEvent<IDalitzEvent>
, virtual public IFastAmplitudeIntegrable
{
 public:
  MINT::counted_ptr<IIntegrationCalculator> makeIntegrationCalculator() override =0;
  MINT::counted_ptr<IntegCalculator> makeIntegCalculator() override =0;

  double RealVal() override =0; // | sum A |^2

  MINT::counted_ptr<MINT::IUnweightedEventGenerator<IDalitzEvent> >
    makeEventGenerator(TRandom* rnd=gRandom) override =0;

  void print(std::ostream& os=std::cout) const override =0;
  virtual void printNonZero(std::ostream& os=std::cout) const=0;

  virtual DalitzBWBoxSet makeBWBoxes(TRandom* rnd=gRandom)=0;

  virtual ~ILookLikeFitAmpSum(){};

};

#endif
//
