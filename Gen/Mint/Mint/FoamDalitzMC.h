/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MINTS_FOAM_DALTIZ_MC_GENERATOR_HH
#define MINTS_FOAM_DALTIZ_MC_GENERATOR_HH

#include "Mint/BaseGenerator.h"
#include "Mint/IDalitzEvent.h"
#include "Mint/IEventGenerator.h"
#include "Mint/IFastAmplitudeIntegrable.h"

#include "Mint/DalitzEventPattern.h"

#include "TFoam.h"

class FoamDalitzMC : BaseGenerator
, virtual public MINT::IEventGenerator<IDalitzEvent>{
 protected:
  TFoam _foam;
  
 public:
  FoamDalitzMC(const DalitzEventPattern& pat, TRandom* rnd=gRandom);
  FoamDalitzMC(IFastAmplitudeIntegrable* amps, TRandom* rnd=gRandom);

  virtual MINT::counted_ptr<IDalitzEvent> tryDalitzEvent();
  virtual MINT::counted_ptr<IDalitzEvent> newDalitzEvent();



  // the below is required by MINT::IEventGenerator<IDalitzEvent>
  virtual MINT::counted_ptr<IDalitzEvent> newEvent()=0;

  virtual bool exhausted() const{return false;}
  virtual bool ensureFreshEvents();

};

#endif
//
