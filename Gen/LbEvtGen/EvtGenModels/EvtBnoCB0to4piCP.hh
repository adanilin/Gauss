//--------------------------------------------------------------------------
//
// Copyright Information: See EvtGen/COPYRIGHT
//
// Module: EvtBnoCB0to4piCP
//
// Description: Model for generating flat phase space for the charmless 
//              region with time-dependent CP violation. Necessary due to 
//              limitations with generator-level cuts in LoKi; inability to 
//              handle Bose-symmetric final states with PHOTOS
//
// Modification history:
//
//    Jeremy Dalseno     Feb 2019     Module created
//
//------------------------------------------------------------------------

#ifndef EVTBNOCB0TO4PICP_HH
#define EVTBNOCB0TO4PICP_HH

#include <string>

#include "EvtGenBase/EvtComplex.hh"
#include "EvtGenBase/EvtDecayAmp.hh"

class EvtParticle;

class EvtBnoCB0to4piCP : public EvtDecayAmp {

public:

  EvtBnoCB0to4piCP();

  std::string getName() override
  { return "BNOCB0TO4PICP"; }

  EvtDecayBase* clone() override
  { return new EvtBnoCB0to4piCP; }

  void initProbMax() override
  { setProbMax(4.0); }

  void init() override;

  void decay(EvtParticle* p) override;

private:

  double _phi2, _Dmd; // Time-dependent CP violation parameters
  double _m2pi_ll, _m2pi_ul; // Lower and upper 2pi invariant mass limits
  double _m3pi_ll, _m3pi_ul; // Lower and upper 3pi invariant mass limits
  double _cPhi2, _sPhi2;
  EvtComplex _I;

};

#endif //EVTBNOCB0TO4PICP_HH
