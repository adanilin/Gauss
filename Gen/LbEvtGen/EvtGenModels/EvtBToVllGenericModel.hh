/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef EVTBTOVLLGENERICMODEL_HH_
#define EVTBTOVLLGENERICMODEL_HH_

#include "EvtBToVllPhysicsModel.hh"
#include "EvtBToVllWC.hh"
#include "EvtBToVllQCDUtils.hh"

namespace qcd{

class GenericModel : public SMPhysicsModel {
public:
	static const std::string modelCommand;
	std::string getModelName() const override {
			return "Generic_Model";
	}
	void setCommand(const std::string& _cmd) override {
		command = _cmd;
	}
	bool hasRightHandedCurrents() const{
		return true;
	}
	qcd::WCPtr getLeftNewPhysicsDeltasMW() const override;
	qcd::WCPtr getRightWilsonCoefficientsMW() const override;
private:
	std::string command;
protected:
	void parseCommand(const WilsonCoefficients<WilsonType>* C,
			const WilsonCoefficients<WilsonType>* CR) const;
};

}

#endif /*EVTBTOVLLGENERICMODEL_HH_*/
