/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PARTICLEGUNS_OTCOSMIC_H
#define PARTICLEGUNS_OTCOSMIC_H

// Include files
// From Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/Transform3DTypes.h"

// From ParticleGuns
#include "LbPGuns/IParticleGunTool.h"

class SolidBox ;
namespace LHCb { class ParticleProperty ; }


/** @class OTCosmic OTCosmic.h "OTCosmic.h"
 * The output will be stored in the transient event store so it can be
 *    passed to the simulation.
 *
 *  @author W. Seligman
 *  @date   2002-11-08
 */
class OTCosmic : public GaudiTool , virtual public IParticleGunTool {
 public:

  /// Constructor
  OTCosmic( const std::string & type , const std::string& name ,
                     const IInterface * parent ) ;

  virtual ~OTCosmic(); ///< Destructor

  /// Initialize method
  StatusCode initialize() override;

  /// Generate particle
  void generateParticle( Gaudi::LorentzVector & fourMomentum ,
                         Gaudi::LorentzVector & origin , int & pdgId ) override;

  /// Print counters
  void printCounters( ) override;

private:

  // event counter, used for event ID
  int m_events, m_generated, m_rejectedbyenergycut, m_rejectedbyscintacceptance;
  float m_emin, m_emax;
  float m_ctcut;
  float m_tmin, m_tmax;
  int m_printEvent, m_printMod;
  float m_thetamin, m_thetamax, m_phimin, m_phimax;

  /// Flat random number generator
  Rndm::Numbers m_flatgenerator ;
  std::unique_ptr<SolidBox> m_scintsolid ;
  Gaudi::Transform3D m_toptransform ;
  Gaudi::Transform3D m_bottransform ;
  const LHCb::ParticleProperty* m_muplus  ;
  const LHCb::ParticleProperty* m_muminus ;
};

#endif
