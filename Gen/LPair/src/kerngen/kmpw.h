/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#if 0
*       for MAC II, MPW shell, LSE compiler
* This pilot patch was created from kernmpw.car patch _kmpw
* This directory was created from kernmpw.car patch qmmpw
* This directory was created from kernfor.car patch qmmpw
*               ISA standard routines, ISHFT, IOR, etc
*                 IEEE floating point
#endif
#ifndef CERNLIB_QMMPW
#define CERNLIB_QMMPW
#endif
#ifndef CERNLIB_F77
#define CERNLIB_F77
#endif
#ifndef CERNLIB_QIEEE
#define CERNLIB_QIEEE
#endif
#ifndef CERNLIB_QISASTD
#define CERNLIB_QISASTD
#endif
#ifndef CERNLIB_QORTHOLL
#define CERNLIB_QORTHOLL
#endif
#ifndef CERNLIB_QINTZERO
#define CERNLIB_QINTZERO
#endif
