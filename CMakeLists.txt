###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
CMAKE_MINIMUM_REQUIRED(VERSION 2.8.12)

#---------------------------------------------------------------
# Load macros and functions for Gaudi-based projects
find_package(GaudiProject)
#---------------------------------------------------------------

macro(FindG4libs)
    # FIXME: this should be an imported target
    foreach(name ${ARGN})
        #message(STATUS "looking for Geant4 ${name} in ${Geant4_LIBRARY_DIRS}")
        find_library(GEANT4_${name}_LIBRARY
                     G4${name}
                     HINTS ${Geant4_DIR}/lib
                     PATHS ${Geant4_LIBRARY_DIRS})
        #message(STATUS "GEANT4_${name}_LIBRARY -> ${GEANT4_${name}_LIBRARY}")
        list(APPEND GEANT4_LIBS ${GEANT4_${name}_LIBRARY})
    endforeach()
    link_directories(${Geant4_LIBRARY_DIRS})
endmacro()

# Declare project name and version
gaudi_project(Gauss v55r2
              FORTRAN
              USE Run2Support v2r0
                  Geant4 v106r2p4
              DATA AppConfig VERSION v3r*
                   BcVegPyData VERSION v4r*
                   Det/GDMLData VERSION v1r*
                   FieldMap VERSION v5r*
                   GenXiccData VERSION v3r*
                   Gen/PGunsData VERSION v1r*
                   Gen/MadgraphData VERSION v20703r6p*
                   Gen/DecFiles VERSION v31r*
                   LHAPDFSets VERSION v62r3
                   MIBData VERSION v3r*
                   ParamFiles VERSION v8r*
                   Vis/XmlVis VERSION v2r*
		   FastCaloSimData VERSION v1r*)
