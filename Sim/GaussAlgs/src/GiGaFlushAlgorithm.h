/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaFlushAlgorithm.h,v 1.4 2007-01-12 15:23:41 ranjard Exp $
#ifndef GIGA_GIGAFLUSHALGORITHM_H
#define GIGA_GIGAFLUSHALGORITHM_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// forward declarations
class IGiGaSvc               ; ///< GiGa


/** @class GiGaFlushAlgorithm GiGaFlushAlgorithm.h
 *
 *  Very simple algorithm to tigger the event processing
 *
 *  @author Vanya Belyaev
 *  @author Gloria Corti
 *  @date   2002-01-22, last modified 2007-01-11
 */
class GiGaFlushAlgorithm : public GaudiAlgorithm
{
public:

  /// Standard constructor
  GiGaFlushAlgorithm( const std::string& Name, ISvcLocator* SvcLoc );

  virtual ~GiGaFlushAlgorithm();  ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

protected:

  /** accessor to GiGa Service
   *  @return pointer to GiGa Service
   */
  inline IGiGaSvc* gigaSvc() const { return m_gigaSvc; }

private:

  std::string     m_gigaSvcName ;
  IGiGaSvc*       m_gigaSvc     ;

};

#endif // GIGA_GIGAFLUSHALGORITHM_H
