/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local
#include "GaussGeoAssembly.h"
#include "GaussGeoAssemblyStore.h"

GaussGeoAssembly::GaussGeoAssembly(const std::string& Name) : m_name(Name), m_volumes() {
  GaussGeoAssemblyStore::store()->addAssembly(this).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
}

GaussGeoAssembly::GaussGeoAssembly(const GaussGeoAssembly& right)
  : m_name(right.m_name),
    m_volumes(right.m_volumes)
{
  GaussGeoAssemblyStore::store()->addAssembly(this).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
}

GaussGeoAssembly::~GaussGeoAssembly() {
  m_volumes.clear();
  GaussGeoAssemblyStore::store()->removeAssembly(this).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
}

StatusCode GaussGeoAssembly::addVolume(const GaussGeoVolumePair& volume_pair, const std::string& name) {
  const GaussGeoVolume& volume = volume_pair.volume();
  if (!volume.isValid()) {
    return StatusCode::FAILURE;
  }

  if (volume.volume() != nullptr) {
    m_volumes.push_back(Volume(NamedG4LVolume(volume.volume(), name), volume_pair.matrix()));
    return StatusCode::SUCCESS;
  }

  const GaussGeoAssembly* assembly = volume.assembly();
  if (assembly == nullptr) {
    return StatusCode::FAILURE;
  }

  for (Volumes::const_iterator volume_iter = assembly->volumes().begin();
       assembly->volumes().end() != volume_iter;
       ++volume_iter) {
    m_volumes.push_back(Volume(NamedG4LVolume(volume_iter->first.first,
                                              name + "#" + volume_iter->first.second),
                                              volume_iter->second * volume_pair.matrix()));
  }

  return StatusCode::SUCCESS ;
}
