###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##########################################################################################################
#
# EM physics study and validation with EMGaussMoni and BremVeloCheck. Configurations that hold information
# for each job.
#
#  Georgios Chatzikonstantinidis 14/05/2017
#  email:georgios.chatzikonstantinidis@cern.ch
#
##########################################################################################################
def config():
	pgunID = 11
	pgunE = 0.1
	emPL = 'NoCuts'
	return {'veloType':'velo','pgunID':pgunID,'pgunE':pgunE,'nEvts': 1000,'dRays':False,'emPL':emPL,
		'saveSim':False,'runGenerator':True,'testType':'both'}
