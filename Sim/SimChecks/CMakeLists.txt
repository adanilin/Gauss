###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: SimChecks
################################################################################
gaudi_subdir(SimChecks v1r8)
 
gaudi_install_scripts()

gaudi_install_python_modules()

gaudi_alias(rad_length_scan rad_length_scan.py)
gaudi_alias(rad_length_scan_velo_z rad_length_scan_velo_z.py)
