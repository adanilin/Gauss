###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
from Gauss.Configuration import *

importOptions('$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py')
importOptions('$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py')
importOptions('$APPCONFIGOPTS/Gauss/DataType-2016.py')
importOptions('$GAUSSOPTS/DBTags-2016.py')
importOptions('$APPCONFIGOPTS/Gauss/RICHRandomHits.py')
importOptions('$DECFILESROOT/options/27162212.py')
importOptions('$LBPYTHIA8ROOT/options/Pythia8.py')
importOptions('$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py')

from Configurables import Gauss
Gauss().SplitSim = True

from Configurables import LHCbApp

GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 1050

LHCbApp().EvtMax = 5
