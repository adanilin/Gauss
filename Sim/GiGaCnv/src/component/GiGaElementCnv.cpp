/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IAddressCreator.h" 
#include "GaudiKernel/IOpaqueAddress.h" 
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/IDataSelector.h"
// DetDesc
#include "DetDesc/Element.h"
#include "DetDesc/Isotope.h"
// GiGa
#include "GiGa/GiGaException.h"
// GiGaCnv
#include "GiGa/IGiGaSetUpSvc.h"
#include "GiGaCnv/GiGaCnvUtils.h"
/// Geant4 includes
#include "Geant4/G4Element.hh"
#include "Geant4/G4Material.hh"
// Local
#include "AddTabulatedProperties.h"
#include "GiGaElementCnv.h"


// ============================================================================
DECLARE_CONVERTER( GiGaElementCnv )
// ============================================================================

// ============================================================================
/// constructor
// ============================================================================
GiGaElementCnv::GiGaElementCnv( ISvcLocator* Locator )
  : GiGaCnvBase( storageType() , classID() , Locator )
  , m_leaf     ( "" , classID() )
{
  setNameOfGiGaConversionService( IGiGaCnvSvcLocation::Geo ) ;
  setConverterName              ( "GiGaElementCnv"         ) ;
}
// ============================================================================

// ============================================================================
/// destructor
// ============================================================================
GiGaElementCnv::~GiGaElementCnv(){}

// ============================================================================
/// Class ID
// ============================================================================
const CLID&  GiGaElementCnv::classID () { return Element::classID() ; }
// ============================================================================

// ============================================================================
/// StorageType
// ============================================================================
unsigned char GiGaElementCnv::storageType ()
{ return GiGaGeom_StorageType; }
// ============================================================================

// ============================================================================
/// Create representation
// ============================================================================
StatusCode GiGaElementCnv::createRep
( DataObject*      Object  ,
  IOpaqueAddress*& Address )
{
  Address = 0 ;
  if( 0 == Object        )
    { return Error("CreateRep::DataObject* point to NULL!");}
  Element* obj = 0 ;
  try        { obj = dynamic_cast<Element*>( Object ) ; }
  catch(...) { obj =                                0 ; }
  if( 0 == obj           )
    { return Error("CreateRep::Bad cast to Element*");}
  if( 0 == cnvSvc()      )
    { return Error("CreateRep::Conversion Service is unavailble ") ;}
  /// create IOpaqueAddress
  IAddressCreator* addrCreator = addressCreator() ;
  if( 0 == addrCreator   )
    { return Error("CreateRep::Address Creator is unavailable   ") ;}
  StatusCode status =
    addrCreator->createAddress( repSvcType  () ,
                                classID     () ,
                                m_leaf.par  () ,
                                m_leaf.ipar () ,
                                Address        );
  if( status.isFailure() )
    { return Error("CreateRep::Error in Address Creator"); }
  if( 0 == Address       )
    { return Error("CreateRep::Address is not created" ); }
  ///
  return updateRep( Address, Object ) ;
  ///
}
// ============================================================================

// ============================================================================
///
// ============================================================================
StatusCode GiGaElementCnv::updateRep
(IOpaqueAddress* /* Address */, DataObject*      Object       )
{
  ///
  typedef Element::Isotopes::iterator Iterator;
  ///
  { MsgStream log( msgSvc() , name() );
  log << MSG::VERBOSE << "UpdateRep::start" << endmsg; }
  ///
  if( 0 == Object   )
    { return Error("UpdateRep::DataObject* points to NULL"); }
  /// extract the name of the element
  const std::string& elementPath = Object->registry()->identifier();
  Element* element = 0 ;
  try        { element = dynamic_cast<Element*>(Object) ; }
  catch(...) { element =                                    0 ; }
  if( 0 == element )
    { return Error("UpdateRep::Bad cast to Element*");}
  /// Check if Element is already known to Geant4
  G4bool warning = false;
  if( 0 != G4Element::GetElement( elementPath , warning ) )
    { return StatusCode::SUCCESS; }

  /// create isotopes first
  {
    for( Iterator it = element->isotopes().begin() ;
         element->isotopes().end() != it ; ++it )
      {
        StatusCode sc = GiGaCnvUtils::createRep( cnvSvc() , it->second ) ;
        if( sc.isFailure()   )
          { return Error("updateRep:: could not convert isotopes for "
                         + elementPath );}
      }

  }

  /// Here we should create the element
  G4Element*  NewElement = 0 ;
  if( 0 == element->nOfIsotopes() ) /// simple element
    { NewElement = new G4Element( elementPath      ,
                                  element->name () ,
                                  element->Z    () ,
                                  element->A    () ); }
  else                        /// compound element (from isotopes)
    {
      ///
      NewElement = new G4Element( elementPath             ,
                                  element->name        () ,
                                  element->nOfIsotopes () ) ;
      for( Iterator it = element->isotopes().begin() ;
           element->isotopes().end() != it ; ++it )
        {
          G4Isotope* isotope =
            G4Isotope::GetIsotope( it->second->registry()->identifier() );
          if( 0 == isotope )
            { return Error("UpdateRep::could not extract isotope=" +
                           it->second->registry()->identifier() ); }
          NewElement->AddIsotope( isotope , it->first ); //
        }
    }

  /// Check if Material is already known to Geant4

  if( 0 != G4Material::GetMaterial( elementPath , warning ) )
    { return StatusCode::SUCCESS; }

  /// Here we could define simple materials
  G4Material*  NewMaterial = 0 ;
  if( 0 == element->nOfIsotopes() )
    {
      NewMaterial = new G4Material( elementPath                 ,
                                    element->Z               () ,
                                    element->A               () ,
                                    element->density         () ,
                                    (G4State) element->state () ,
                                    element->temperature     () ,
                                    element->pressure        () );
    }
  else
    {
      ///
      NewMaterial = new G4Material( elementPath                 ,
                                    element->density         () ,
                                    element->nOfIsotopes     () ,
                                    (G4State) element->state () ,
                                    element->temperature     () ,
                                    element->pressure        () );
      for( Iterator it = element->isotopes().begin() ;
           element->isotopes().end() != it ; ++it )
        {
          G4Element* el =
            G4Element::GetElement( it->second->registry()->identifier() );
          if( 0 == el )
            { return Error("UpdateRep::could not extract element=" +
                           it->second->registry()->identifier() ); }
          NewMaterial->AddElement( el , it->first ); //
        }
    }
  /// add tabulated properties
  if( !element->tabulatedProperties().empty() )
    {
      if( 0 == NewMaterial->GetMaterialPropertiesTable() )
        { NewMaterial->
            SetMaterialPropertiesTable( new G4MaterialPropertiesTable() ); }
      StatusCode sc =
        AddTabulatedProperties ( element->tabulatedProperties()  ,
                                 NewMaterial->GetMaterialPropertiesTable() ) ;
      if( sc.isFailure() )
        { return Error("UpdateRep::could not add TabulatedProperties for " +
                       elementPath , sc  ); }
    }
  ///
  { MsgStream log( msgSvc() , name() );
  log << MSG::VERBOSE << "UpdateRep::end" << endmsg; }
  ///
  return StatusCode::SUCCESS;
  ///
}


// ============================================================================
// End
// ============================================================================
















