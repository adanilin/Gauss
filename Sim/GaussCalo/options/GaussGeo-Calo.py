###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Input Histograms
from Gaudi.Configuration import importOptions
from Configurables import HistogramDataSvc
HistogramDataSvc().Input += ["GaussCalo DATAFILE='$PARAMFILESROOT/data/gausscalo.root' TYP='ROOT'"]

# Configuration of Spd sensitive detector 
importOptions("$GAUSSCALOROOT/options/GaussGeo-Spd.opts")

# Configuration of Prs sensitive detector 
importOptions("$GAUSSCALOROOT/options/GaussGeo-Prs.opts")

# Configuration of Ecal sensitive detector 
importOptions("$GAUSSCALOROOT/options/GaussGeo-Ecal.opts")

# Configuration of Hcal sensitive detector 
importOptions("$GAUSSCALOROOT/options/GaussGeo-Hcal.opts")

# Configuration Sensitive plane detector 
importOptions("$GAUSSCALOROOT/options/GaussGeo-CaloSP.opts")
