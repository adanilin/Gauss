/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
// LHCb
#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/MCCaloHit.h"
#include "Event/MCExtendedHit.h"
#include "Event/MCHeader.h"
#include "Event/MCHit.h"
// Gauss
#include "GaussTools/ZMaxPlane.h"

class EcalCollector
    : public Gaudi::Functional::Consumer<void( const LHCb::MCHits&, const LHCb::MCCaloHits&, const LHCb::MCHeader&,
                                               const DeCalorimeter& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>> {

public:
  EcalCollector( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {KeyValue{"CollectorHits", ""}, KeyValue{"CaloHits", LHCb::MCCaloHitLocation::Ecal},
                   KeyValue{"MCHeader", LHCb::MCHeaderLocation::Default},
                   KeyValue{"CaloLocation", DeCalorimeterLocation::Ecal}} ){};

  void operator()( const LHCb::MCHits&, const LHCb::MCCaloHits&, const LHCb::MCHeader&,
                   const DeCalorimeter& ) const override;

  StatusCode initialize() override;

private:
  mutable Gaudi::Accumulators::Counter<> m_caloHits{this, "CaloHits"};
  mutable Gaudi::Accumulators::Counter<> m_collHits{this, "CollectorHits"};

  ZMaxPlane m_collectorPlane;

  // set this on whenever there is a possibility that a particle stored in MCCaloHits is a stored daughter of a particle
  // registered in the collector;
  Gaudi::Property<bool>   m_checkParent{this, "CheckParent", false};
  Gaudi::Property<double> m_collZ{this, "CollectorZ", 0.};
  Gaudi::Property<double> m_collAngle{this, "CollectorXAngle", 0.};
  Gaudi::Property<double> m_collYShift{this, "CollectorYShift", 0.};
};

StatusCode EcalCollector::initialize() {
  return Consumer::initialize().andThen( [&] {
    if ( m_checkParent.value() ) {
      m_collectorPlane.prepare( m_collZ.value(), m_collAngle.value(), m_collYShift.value() );
    }
  } );
}

void EcalCollector::operator()( const LHCb::MCHits& collHits, const LHCb::MCCaloHits& caloHits,
                                const LHCb::MCHeader& evt, const DeCalorimeter& calo ) const {

  Tuple caloT = nTuple( "Hits" );

  for ( const auto& caloHit : caloHits ) {
    caloT->column( "Event_ID", evt.evtNumber() ).ignore();
    auto particle = caloHit->particle();
    if ( m_checkParent.value() ) {
      auto vrxPos = particle->originVertex()->position();
      while ( m_collectorPlane.Distance( vrxPos.y(), vrxPos.z() ) > 0. ) {
        if ( auto mother = particle->mother(); mother ) {
          particle = mother;
          vrxPos   = particle->originVertex()->position();
        } else {
          warning() << "No information about particle's mother!" << endmsg;
          break;
        }
      }
    }
    caloT->column( "Particle_Index", particle->index() ).ignore();
    caloT->column( "Active_Energy", caloHit->activeE() ).ignore();
    auto cell_id = caloHit->cellID();
    caloT->column( "Cell_ID", cell_id.index() ).ignore();
    caloT->column( "Cell_X", calo.cellX( cell_id ) ).ignore();
    caloT->column( "Cell_Y", calo.cellY( cell_id ) ).ignore();
    caloT->column( "Cell_Z", calo.cellZ( cell_id ) ).ignore();
    caloT->column( "Cell_Size", calo.cellSize( cell_id ) ).ignore();

    auto sc = caloT->write();
    if ( sc.isFailure() ) { throw GaudiException( "NTuple not wrtitten!", "EcalCollector", StatusCode::FAILURE ); }
    ++m_caloHits;
  }

  Tuple collT = nTuple( "Particles" );

  for ( const auto& collHit : collHits ) {
    auto extHit = dynamic_cast<LHCb::MCExtendedHit* const>( collHit );
    auto entry  = extHit->entry();
    collT->column( "Event_ID", evt.evtNumber() ).ignore();
    collT->column( "Entry_X", entry.X() ).ignore();
    collT->column( "Entry_Y", entry.Y() ).ignore();
    collT->column( "Entry_Z", entry.Z() ).ignore();
    auto mom = extHit->momentum();
    collT->column( "Momentum", extHit->p() ).ignore();
    collT->column( "Momentum_X", mom.X() ).ignore();
    collT->column( "Momentum_Y", mom.Y() ).ignore();
    collT->column( "Momentum_Z", mom.Z() ).ignore();
    auto particle = extHit->mcParticle();
    collT->column( "Particle_Index", particle->index() ).ignore();
    collT->column( "Particle_PID", particle->particleID().pid() ).ignore();

    auto sc = collT->write();
    if ( sc.isFailure() ) { throw GaudiException( "NTuple not wrtitten!", "EcalCollector", StatusCode::FAILURE ); }
    ++m_collHits;
  }
}

DECLARE_COMPONENT( EcalCollector )
