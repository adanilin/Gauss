###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LbGDML
################################################################################
gaudi_subdir(LbGDML v1r0p2)

gaudi_depends_on_subdirs(GaudiAlg
                         Sim/GaussTools)

FindG4libs(persistency)

find_package(XercesC)

find_package(Boost)
find_package(CLHEP)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(LbGDML
                 src/*.cpp
                 INCLUDE_DIRS XercesC
                 LINK_LIBRARIES GaudiAlgLib GaussToolsLib
                                XercesC
                                ${GEANT4_LIBS}
                 GENCONF_PRELOAD GaussToolsGenConfHelperLib)

