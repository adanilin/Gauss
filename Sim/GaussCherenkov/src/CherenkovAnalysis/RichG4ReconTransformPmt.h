/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichG4ReconTransformHpd.h,v 1.6 2008-05-30 13:43:23 gcorti Exp $
#ifndef RICHANALYSIS_RICHG4RECONTRANSFORMPMT_H
#define RICHANALYSIS_RICHG4RECONTRANSFORMPMT_H 1
// Include files
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/ILVolume.h"
#include "DetDesc/IPVolume.h"
#include "DetDesc/Material.h"
#include "GaudiKernel/Transform3DTypes.h"
 

/** @class RichG4ReconTransformPmt RichG4ReconTransformPmt.h RichAnalysis/RichG4ReconTransformPmt.h
 *
 *
 *
 *  @author Sajan EASO
 *  @date   2003-09-09
 */
class RichG4ReconTransformPmt {

public:

  /// Standard constructor
  RichG4ReconTransformPmt( );

  RichG4ReconTransformPmt(int aRichDetNum, int PmtModuleNum, int aPmtNum );

  virtual ~RichG4ReconTransformPmt( ); ///< Destructor

  const Gaudi::Transform3D & PmtLocalToGlobal()
  {
    return m_PmtLocalToGlobal;
  }

  const Gaudi::Transform3D & PmtGlobalToLocal()
  {
    return m_PmtGlobalToLocal;
  }
  const Gaudi::Transform3D & PmtPhDetPanelToLocal() 
  {  return m_PmtPhDetPanelToLocal;}
  const Gaudi::Transform3D & PmtLocalToPmtPhDetPanel()
  {   return m_PmtLocalToPmtPhDetPanel;}
  
    
  
  void initialise();
protected:

private:

  Gaudi::Transform3D m_PmtLocalToGlobal;
  Gaudi::Transform3D m_PmtGlobalToLocal;

  Gaudi::Transform3D m_PmtPhDetPanelToLocal;
  Gaudi::Transform3D m_PmtLocalToPmtPhDetPanel;
  
  int   m_Rich1SubMasterPvIndex;
  int   m_PmtSMasterIndex;

  std::string m_Rich1MagShPvName0;
  std::string m_Rich1MagShPvName1;
  std::string m_Rich1PhtDetSupName0;
  std::string m_Rich1PhtDetSupName1;
  std::string m_Rich2PmtPanelName0;
  std::string m_Rich2PmtPanelName1;
  std::string m_Rich2PmtN2EnclName0;
  std::string m_Rich2PmtN2EnclName1;
  
  std::string m_Rich1PmtStdModuleName;
  std::string m_Rich1PmtNoEC0TypeModuleName;
  std::string m_Rich1PmtNoEC3TypeModuleName;
  std::string m_Rich1PmtNoEC01TypeModuleName;
  std::string m_Rich1PmtNoEC23TypeModuleName;
  std::string m_Rich2PmtStdModuleName;
  std::string m_Rich2PmtGrandModuleName;

  std::vector<std::string> m_Rich1PmtStdECName;
  std::vector<std::string> m_Rich1PmtStdECInNoEC0TypeModuleName;
  std::vector<std::string> m_Rich1PmtStdECInNoEC3TypeModuleName;
  std::vector<std::string> m_Rich1PmtStdECInNoEC01TypeModuleName;
  std::vector<std::string> m_Rich1PmtStdECInNoEC23TypeModuleName;
  std::vector<std::string> m_Rich2PmtStdECName;
  std::vector<std::string> m_Rich2PmtGrandECName;
  
  int m_Rich1PmtModuleMaxH0;
  int m_Rich2PmtModuleMaxH0;
  int m_RichNumPmtInModule;
  int m_RichMaxNumEcInModule;
  int m_RichMaxNumPmtInEc;
  int m_Rich1NumberOfModulesInRow;
  int m_Rich1NumberOfModulesInCol;

};
#endif // RICHANALYSIS_RICHG4RECONTRANSFORMPMT_H
