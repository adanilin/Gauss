/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef CHERENKOVTESTBEAMSENSDET_TORCHTBMCPSENSDET_H
#define CHERENKOVTESTBEAMSENSDET_TORCHTBMCPSENSDET_H 1

// Include files
// from GiGa
#include "GiGa/GiGaSensDetBase.h"

#include "GaussCherenkov/CkvG4Hit.h"
#include "TorchTBHitCollName.h"
#include "Geant4/G4Step.hh"

// forward declarations
class G4HCofThisEvent;



/** @class TorchTBMcpSensDet TorchTBMcpSensDet.h CherenkovTestBeamSensDet/TorchTBMcpSensDet.h
 *
 *
 *  @author Sajan Easo
 *  @date   2012-05-25
 */


class TorchTBMcpSensDet : virtual public GiGaSensDetBase{

public:
  /// Standard constructor
  TorchTBMcpSensDet(const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent );

  ~TorchTBMcpSensDet( ); ///< Destructor

 /// initialize
  StatusCode initialize() override;

  /// finalize
  StatusCode finalize() override;

  void Initialize(G4HCofThisEvent* HCE) override;
  bool ProcessHits ( G4Step* step , G4TouchableHistory* history ) override;
  void clear() override;

protected:

private:
  TorchTBMcpSensDet();
  TorchTBMcpSensDet(const TorchTBMcpSensDet& );
  TorchTBMcpSensDet& operator = (const TorchTBMcpSensDet& );
  TorchTBHitCollName * m_TorchTBHitCollName;
  CkvG4HitsCollection* m_TorchHC;
  G4int m_torchHCID;

private:


};
#endif // CHERENKOVTESTBEAMSENSDET_TORCHTBMCPSENSDET_H
