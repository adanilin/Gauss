/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4AntiXiccStarMinus_h
#define G4AntiXiccStarMinus_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         AntiXiccStarMinus                        ###
// ######################################################################

class G4AntiXiccStarMinus : public G4ParticleDefinition
{
 private:
  static G4AntiXiccStarMinus * theInstance ;
  G4AntiXiccStarMinus( ) { }
  ~G4AntiXiccStarMinus( ) { }


 public:
  static G4AntiXiccStarMinus * Definition() ;
  static G4AntiXiccStarMinus * AntiXiccStarMinusDefinition() ;
  static G4AntiXiccStarMinus * AntiXiccStarMinus() ;
};


#endif
